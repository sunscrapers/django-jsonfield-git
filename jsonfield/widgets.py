from django import forms
from django.utils import simplejson as json
from django import utils

import jsonfield.fields


class JSONWidget(forms.Textarea):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ""
        if not isinstance(value, basestring):
            value = json.dumps(value, indent=2, default=jsonfield.fields.default)
        return super(JSONWidget, self).render(name, value, attrs)


class JSONSelectWidget(forms.SelectMultiple):
    pass


class JSONEditableWidget(forms.Textarea):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ""
        if not isinstance(value, basestring):
            value = json.dumps(value, indent=2, default=jsonfield.fields.default)
        html = u"""
            <div class="json-editable-container">
                <div class="json-editable-input">{input}</div>
                <div class="json-editable-fields"></div>
            </div>
        """.format(input=super(JSONEditableWidget, self).render(name, value, attrs))
        return utils.safestring.mark_safe(u"" + html)

    class Media:
        js = ('jsonfield/js/JSONEditableWidget.js', )