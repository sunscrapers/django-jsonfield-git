
$(document).ready(function() {

    function createInput(text) {
        return '<input type="text" value="'+ text + '" />';
    }
    function createTextarea(text) {
        return '<textarea rows="1" cols="50" style="max-width: 300px;">' + text + '</textarea>';
    }
    function createRemoveLink() {
        return '<a class="json-editable-remove" style="cursor: pointer;"><b>(–)</b> Remove</a>';
    }
    function createRow(key, val) {
        return '<tr>' +
            '<td>' + createInput(key) + '</td>' +
            '<td>' + createTextarea(val) + '</td>' +
            '<td>' + createRemoveLink() + '</td>' +
        '</tr>';
    }
    function createAddLink() {
        return '<a class="json-editable-add" style="cursor: pointer;"><b>(+)</b> Add new</a>';
    }

    $('.json-editable-container').each(function() {
        var jsonfield = $(this).find('.json-editable-input textarea');
        $('.json-editable-input').hide();

        var fields = $('<table/>');
        fields.appendTo($(this).find('.json-editable-fields'));

        function fields_changed() {
            var newjson = {}
            fields.find('tr').each(function() {
                var key = $(this).find('input').val();
                var val = $(this).find('textarea').val();
                if (key && val) {
                    newjson[key] = val;
                }
            });
            jsonfield.val(JSON.stringify(newjson));
        }
        function remapStructure() {
            fields.find('textarea').keyup(function() {
                fields_changed();
            });
            fields.find('.json-editable-remove').click(function() {
                $(this).parent().parent().remove();
                fields_changed();
            });
        }

        // Build structure
        fields.append('<tr><td>Key</td><td>Value</td><td></td></tr>');
        $.each(JSON.parse(jsonfield.val()), function(key, val) {
            fields.append(createRow(key, val));
            remapStructure();
        });
        fields.append($('<tr><td>' + createAddLink() + '</td></tr>'));

        // Add listeners
        fields.find('.json-editable-add').click(function() {
            fields.find('tr:last').before(createRow('', ''));
            remapStructure();
        });
        remapStructure();

    });
});
